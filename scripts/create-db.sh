# run this as postgres user, eg:
# imposm-psqldb > create_db.sh; sudo su postgres; sh ./create_db.sh
set -xe
createuser --no-superuser --no-createrole --createdb osm-adav
createdb -E UTF8 -O osm-adav osm-adav
#createlang plpgsql osm-adav
echo "CREATE EXTENSION postgis;" | psql -d osm-adav
echo "ALTER TABLE geometry_columns OWNER TO \"osm-adav\";" | psql -d osm-adav
psql -d osm-adav -f /usr/local/lib/python2.7/dist-packages/imposm/900913.sql
echo "ALTER TABLE spatial_ref_sys OWNER TO \"osm-adav\";" | psql -d osm-adav
echo "ALTER USER osm-adav WITH PASSWORD \"\";" |psql -d osm-adav
echo "host	osm-adav	osm-adav	127.0.0.1/32	md5" >> /etc/postgresql/9.1/main/pg_hba.conf
set +x
echo "Done. Don't forget to restart postgresql!"
