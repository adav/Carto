#!/bin/sh

wget http://download.geofabrik.de/europe/france/nord-pas-de-calais-latest.osm.pbf -O /home/adav/data/nord-pas-de-calais-latest.osm.pbf
imposm --connection=postgis://osm-adav@localhost:5432/osm-adav --read --write -m /home/adav/scripts/mapping.py --read /home/adav/data/nord-pas-de-calais-latest.osm.pbf --overwrite-cache
cd /home/adav/Documents/MapBox/project/ADAV-Tilemill/
git pull
rm /home/adav/mbtiles/adav.mbtiles
/usr/bin/nodejs /usr/share/tilemill/index.js export ADAV-Tilemill /home/adav/mbtiles/adav.mbtiles --format=mbtiles --bbox="1.5556,50.0158,4.1923,51.0924" --minzoom=8 --maxzoom=18
