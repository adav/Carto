/* jslint vars: true */
/*jslint indent: 3 */
/* global $, L, currentUser, Mustache, routie */
'use strict';

var cyclab_map = function() {
	var segments_by_intersection;
	var cyclab_map_initialised = false;

	var cities = {
		'dunkerque' : [51.0342,2.3775,15],
		'hazebrouck' : [50.7247,2.5404,15],
		'armentières' : [50.6910,2.8831,15],
		'lille' : [50.6290,3.0702,14],
		'roubaix' : [50.6900,3.1736,14],
		'tourcoing' : [50.7192,3.1575,14],
		'douai' : [50.3697,3.0747,15],
		'valenciennes' : [50.3585,3.5256,15],
		'maubeuge' : [50.2788,3.9742,16],
		'cambrai' : [50.1764,3.2349,15]
	};

	/*
	 * Center the map on a given city center. If the map is not alreay
	 * initialised (drawn), this function will do it.
	 * @param {String} city The name of the city
	 * @param {int} zoom_level The zoom level (optional)
	 * @return No return
	 */
	function goToCity(city,zoom_level) {
		city = city.toLowerCase();

		if (!(city in cities)) {
			city = 'lille';
		}

		if(!zoom_level) {
			zoom_level = cities[city][2];
		}

		if(cyclab_map_initialised) {
			map.setView(cities[city], zoom_level);
		} else {
			init(cities[city][0],cities[city][1],zoom_level);
		}
	}

	/*
	 * Center the map on a given longitude/latitude. If the map is not alreay
	 * initialised (drawn), this function will do it.
	 * @param {float} lat The latitude
	 * @param {float} lon The longitude
	 * @param {int} zoom_level The zoom level (optional)
	 * @return No return
	 */
	function gotToLonLatLevel(lat,lon,zoom_level) {
		if(cyclab_map_initialised) {
			if(!zoom_level) {
				zoom_level = map.getZoom();
			}
			map.setView([lat, lon], zoom_level);
		} else {
			if(!zoom_level) {
				zoom_level = display_entity_min_zoom_level;
			}
			init(lat,lon,zoom_level);
		}
	}

	/**
	 * Initialization of the cyclab map
	 * @param {float} lat The latitude of the center of the map
	 * @param {float} lon The longitude of the center of the map
	 * @param {integer} zoom The zoom limit (when it is possible to display entities on the map or not)
	 */
	function init(lat, lon, min_zoom_level) {
		cyclab_map_initialised = true;
		var map;

		var hillShade = new L.TileLayer("http://toolserver.org/~cmarqu/hill/{z}/{x}/{y}.png", {
			maxZoom: 17,
			attribution: 'Tuiles de fond <a href="http://www.openstreetmap.org/" target="_blank">OSM.org</a>',
			opacity: 0.7
		});

		var blackandwhiteOSM = new L.TileLayer("http://www.toolserver.org/tiles/bw-mapnik/{z}/{x}/{y}.png", {
			attribution: 'Tuiles de fond <a href="http://www.openstreetmap.org/" target="_blank">OSM.org</a>',
			opacity: 0.7
		});

		var mapquestOSM = new L.TileLayer("http://{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png", {
			subdomains: ["otile1", "otile2", "otile3", "otile4"],
			attribution: 'Tuiles de fond <a href="http://www.mapquest.com/" target="_blank">MapQuest</a>',
			opacity: 0.7
		});

		var elevation = new L.tileLayer("http://{s}.tile.cartosm.eu/tile/isohypse/{z}/{x}/{y}.png", {
			subdomains: ["a", "b", "c"],
			attribution: 'Elevation : <a href="http://blog.rodolphe.quiedeville.org/" target="_blank">Rodolphe Quiédeville</a>',
			opacity: 0.7
		});

		var mbTiles = new L.tileLayer('mbtiles-server/mbtiles.php?db=db/adav.mbtiles&z={z}&x={x}&y={y}', {
			tms: true,
			attribution: 'Équipement vélo <a href="http://www.droitauvelo.org" target="_blank">ADAV</a>',
			opacity: 0.7
		});

		var baseLayers = {
			"MapQuest Streets": mapquestOSM,
			"OSM Monochrome": blackandwhiteOSM
		};
		var overlays = {
			"Données vélo": mbTiles,
			"Courbes de niveau": elevation,
			"Ombrage du relief": hillShade
		};

					map = new L.Map("map",{
						zoom: 9,
						center: [50.6146,3.0652],
						layers: [hillShade,mapquestOSM, mbTiles]
					});
					L.control.layers(baseLayers, overlays).addTo(map);
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(
				function (position) {
					map = new L.Map("map",{
						zoom: 16,
						center: [position.coords.latitude,position.coords.longitude],
						layers: [hillShade,mapquestOSM, mbTiles]
					})
					L.control.layers(baseLayers, overlays).addTo(map);
				},
				function (error) {
					map = new L.Map("map",{
						zoom: 9,
						center: [50.6146,3.0652],
						layers: [hillShade,mapquestOSM, mbTiles]
					});
					L.control.layers(baseLayers, overlays).addTo(map);
				});
		}
		else {
			map = new L.Map("map",{
				zoom: 9,
				center: [50.6146,3.0652],
				layers: [hillShade,mapquestOSM, mbTiles]
			});
			L.control.layers(baseLayers, overlays).addTo(map);
		}

	}
} ();

$(document).ready(function() {
	routie({
		'map/:lat/:lon/:zoom?': cyclab_map.gotToLonLatLevel,
		':city/:zoom?': cyclab_map.goToCity,
		'': function() {
			cyclab_map.goToCity('Lille');
		}
	});
	$('#town_choice_link').colorbox({
		inline:true,
		width:'400px',
		height:'400px',
	});
});
