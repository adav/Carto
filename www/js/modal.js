$('.modal').draggable();
$('.modal').resizable({
  //alsoResize: ".modal-dialog",
  minHeight: 300,
  minWidth: 300
});
$('#myModal').on('show.bs.modal', function() {
  $(this).find('.modal-body').css({
    'max-height': '100%'
  });
});

$(function() {
  $("#accordion").accordion().hide();
  $('#clicker').button().click(function(){
    var overlayDialogObj = {
      autoOpen: true,
      height: 400,
      width: 310,
      modal: false,
      open: function(){
        $('#accordion').accordion(
          {heightStyle: "fill", collapsible: true}).show();
        },
        buttons: {
          'Done': function() {
            $(this).dialog('close');
          }
        }
      };
      $('#dialog').dialog(overlayDialogObj).show();

    });
  });
